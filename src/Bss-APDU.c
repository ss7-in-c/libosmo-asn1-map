/*
 * Generated by asn1c-0.9.21 (http://lionet.info/asn1c)
 * From ASN.1 module "DummyMAP"
 * 	found in "../asn//GSMMAP.asn"
 */

#include <asn_internal.h>

#include "Bss-APDU.h"

static asn_TYPE_member_t asn_MBR_Bss_APDU_1[] = {
	{ ATF_NOFLAGS, 0, offsetof(struct Bss_APDU, protocolId),
		(ASN_TAG_CLASS_UNIVERSAL | (10 << 2)),
		0,
		&asn_DEF_ProtocolId,
		0,	/* Defer constraints checking to the member type */
		0,	/* PER is not compiled, use -gen-PER */
		0,
		"protocolId"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct Bss_APDU, signalInfo),
		(ASN_TAG_CLASS_UNIVERSAL | (4 << 2)),
		0,
		&asn_DEF_SignalInfo,
		0,	/* Defer constraints checking to the member type */
		0,	/* PER is not compiled, use -gen-PER */
		0,
		"signalInfo"
		},
	{ ATF_POINTER, 1, offsetof(struct Bss_APDU, extensionContainer),
		(ASN_TAG_CLASS_UNIVERSAL | (16 << 2)),
		0,
		&asn_DEF_ExtensionContainer,
		0,	/* Defer constraints checking to the member type */
		0,	/* PER is not compiled, use -gen-PER */
		0,
		"extensionContainer"
		},
};
static ber_tlv_tag_t asn_DEF_Bss_APDU_tags_1[] = {
	(ASN_TAG_CLASS_UNIVERSAL | (16 << 2))
};
static asn_TYPE_tag2member_t asn_MAP_Bss_APDU_tag2el_1[] = {
    { (ASN_TAG_CLASS_UNIVERSAL | (4 << 2)), 1, 0, 0 }, /* signalInfo at 317 */
    { (ASN_TAG_CLASS_UNIVERSAL | (10 << 2)), 0, 0, 0 }, /* protocolId at 316 */
    { (ASN_TAG_CLASS_UNIVERSAL | (16 << 2)), 2, 0, 0 } /* extensionContainer at 318 */
};
static asn_SEQUENCE_specifics_t asn_SPC_Bss_APDU_specs_1 = {
	sizeof(struct Bss_APDU),
	offsetof(struct Bss_APDU, _asn_ctx),
	asn_MAP_Bss_APDU_tag2el_1,
	3,	/* Count of tags in the map */
	0, 0, 0,	/* Optional elements (not needed) */
	2,	/* Start extensions */
	4	/* Stop extensions */
};
asn_TYPE_descriptor_t asn_DEF_Bss_APDU = {
	"Bss-APDU",
	"Bss-APDU",
	SEQUENCE_free,
	SEQUENCE_print,
	SEQUENCE_constraint,
	SEQUENCE_decode_ber,
	SEQUENCE_encode_der,
	SEQUENCE_decode_xer,
	SEQUENCE_encode_xer,
	0, 0,	/* No PER support, use "-gen-PER" to enable */
	0,	/* Use generic outmost tag fetcher */
	asn_DEF_Bss_APDU_tags_1,
	sizeof(asn_DEF_Bss_APDU_tags_1)
		/sizeof(asn_DEF_Bss_APDU_tags_1[0]), /* 1 */
	asn_DEF_Bss_APDU_tags_1,	/* Same as above */
	sizeof(asn_DEF_Bss_APDU_tags_1)
		/sizeof(asn_DEF_Bss_APDU_tags_1[0]), /* 1 */
	0,	/* No PER visible constraints */
	asn_MBR_Bss_APDU_1,
	3,	/* Elements count */
	&asn_SPC_Bss_APDU_specs_1	/* Additional specs */
};

