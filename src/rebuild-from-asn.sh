#!/bin/bash
ASN=../asn/
asn1c -pdu=auto $ASN/GSMMAP.asn $ASN/MAP-MS-DataTypes.asn $ASN/MAP-SS-DataTypes.asn $ASN/MAP-CommonDataTypes.asn $ASN/MAP-ExtensionDataTypes.asn $ASN/MAP-SS-Code.asn $ASN/MAP-BS-Code.asn $ASN/MAP-TS-Code.asn $ASN/MAP-ER-DataTypes.asn $ASN/MAP-OM-DataTypes.asn $ASN/MAP-CH-DataTypes.asn $ASN/MAP-DialogueInformation.asn $ASN/MAP-SM-DataTypes.asn
find . -type l -exec rm \{\} \;
mv *.h ../include
