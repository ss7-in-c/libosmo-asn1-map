/*
 * Generated by asn1c-0.9.21 (http://lionet.info/asn1c)
 * From ASN.1 module "MAP-OM-DataTypes"
 * 	found in "../asn//MAP-OM-DataTypes.asn"
 */

#ifndef	_DeactivateTraceModeRes_H_
#define	_DeactivateTraceModeRes_H_


#include <asn_application.h>

/* Including external dependencies */
#include <constr_SEQUENCE.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Forward declarations */
struct ExtensionContainer;

/* DeactivateTraceModeRes */
typedef struct DeactivateTraceModeRes {
	struct ExtensionContainer	*extensionContainer	/* OPTIONAL */;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
	
	/* Context for parsing across buffer boundaries */
	asn_struct_ctx_t _asn_ctx;
} DeactivateTraceModeRes_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_DeactivateTraceModeRes;

#ifdef __cplusplus
}
#endif

/* Referred external types */
#include "ExtensionContainer.h"

#endif	/* _DeactivateTraceModeRes_H_ */
