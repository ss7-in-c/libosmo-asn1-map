/*
 * Generated by asn1c-0.9.21 (http://lionet.info/asn1c)
 * From ASN.1 module "MAP-MS-DataTypes"
 * 	found in "../asn//MAP-MS-DataTypes.asn"
 */

#ifndef	_Bandwidth_H_
#define	_Bandwidth_H_


#include <asn_application.h>

/* Including external dependencies */
#include <INTEGER.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Bandwidth */
typedef INTEGER_t	 Bandwidth_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_Bandwidth;
asn_struct_free_f Bandwidth_free;
asn_struct_print_f Bandwidth_print;
asn_constr_check_f Bandwidth_constraint;
ber_type_decoder_f Bandwidth_decode_ber;
der_type_encoder_f Bandwidth_encode_der;
xer_type_decoder_f Bandwidth_decode_xer;
xer_type_encoder_f Bandwidth_encode_xer;

#ifdef __cplusplus
}
#endif

#endif	/* _Bandwidth_H_ */
